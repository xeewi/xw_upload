<?php 

class xw_upload {

/*	Proprieties
----------------------- */
	private $_dir;
	private $_from;
	private $_ext;
	private $_file_name;
	private $_max_size;

/*	Construct
----------------------- */
	public function __construct($from, $dir){
		$this->set_from($from);
		$this->set_dir($dir);
	}

/*	Getters
----------------------- */
	public function dir(){ return $this->_dir; }
	public function from(){ return $this->_from; }
	public function ext(){ return $this->_ext; }
	public function file_name(){ return $this->_file_name; }
	public function max_size(){ return $this->_max_size; }

/*	Setters
----------------------- */
	public function set_dir( $value ){
		if (is_string($value)) {
			$this->_dir = $value;
		}
	}

	public function set_from( $value ){
		if (is_string($value)) {
			$this->_from = $value;
		}
	}

	public function set_ext( $value ){
		if (is_string($value)) {
			$this->_ext = $value;
		}
	}

	public function set_file_name( $value ){
		if (is_string($value)) {
			$this->_file_name = $value;
		}
	}

	public function set_max_size( $value ){
		if (is_int($value)) {
			$this->_max_size = $value;
		}
	}

/*	Functions
----------------------- */
	public function single_upload($file_name = false, $max_size = false){
		if ( $file_name == false){ $this->set_file_name( uniqid('up_') ); } else { $this->set_file_name($file_name); }
		if ( $max_size == false ) { $this->set_max_size( 3 * 1000 * 1000 ); } else { $this->set_max_size($max_size * 1000 * 1000); }

		if( !isset($_FILES[$this->from()]) ) { throw new Exception( 'No file from ' . $this->from() ); return false; }
		if( is_array( $_FILES[$this->from()]['name'] ) ) { throw new Exception( 'Multiple files from ' . $this->from ); return false; }
		if( $_FILES[$this->from()]['size'] > $this->max_size() ) { throw new Exception( 'File too large' ); return false; }
		if( file_exists($this->dir() . basename($_FILES[$this->from()]["name"]) ) ) { throw new Exception( 'File already exists' ); return false; }


		$this->set_ext( pathinfo( $_FILES[$this->from()]['name'], PATHINFO_EXTENSION ) );

		move_uploaded_file( $_FILES[$this->from()]["tmp_name"], $this->dir() . $this->file_name() . '.' . $this->ext() );

		return $this->file_name() . '.' . $this->ext();
	}

	public function multi_upload($file_name = false, $max_size = false){
		if ( $max_size == false ) { $this->set_max_size( 3 * 1000 * 1000 ); } else { $this->set_max_size($max_size * 1000 * 1000); }	
		if( !isset($_FILES[$this->from()]) ) { throw new Exception( 'No file from ' . $this->from() ); return false; }

		foreach ($_FILES[$this->from()]['size'] as $key => $value) {
			if( $_FILES[$this->from()]['size'][$key] > $this->max_size() ) { throw new Exception( 'File too large' ); return false; }
		}

		$ups = [];

		foreach ($_FILES[$this->from()]['tmp_name'] as $key => $value) {
			if ( $file_name == false){ $this->set_file_name( uniqid('up_') ); } else { $this->set_file_name( uniqid($file_name) ); }
			$this->set_ext( pathinfo( $_FILES[$this->from()]['name'][$key], PATHINFO_EXTENSION ) );

			move_uploaded_file( $_FILES[$this->from()]["tmp_name"][$key], $this->dir() . $this->file_name() . '.' . $this->ext() );
			array_push($ups, $this->file_name() . '.' . $this->ext());
		}

		return $ups;
		
	}

}
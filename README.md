# PHP Upload file

## General

```
$upload = new xw_upload(string $from, string $dir);
```

`$from` is the name of the `<input type="file>`

`$dir` is the path to your uploads directory **(you must create it before)**


## Single upload

[Click here to try online](http://gautierg.etudiant-eemi.com/3A_dev/xw_upload/test/single_upload.php)

```
$upload->single_upload(string $name = false, string $max_size = false);
```

`$name` is the name you want to give to the file.

Default names are : "up_" + unique id.

`$max_size` is the max size of the file (in Mo).

Default max size is 3Mo. 

Return file name (string) if success, Exception if failed.


## Multiple upload

[Click here to try online](http://gautierg.etudiant-eemi.com/3A_dev/xw_upload/test/multi_upload.php)

```
$upload->multi_upload(string $name = false, string $max_size = false);
```

Do not forget to format your `<input type="file">` name's like this : name="myname**[]**"

`$name` is the name you want to give to the files, it will be format like this : "myname" + unique id.

Default names are : "up_" + unique id.

`$max_size` is the max size of the file (in Mo).

Default max size is 3Mo. 

Return file names (array) if success, Exception if failed
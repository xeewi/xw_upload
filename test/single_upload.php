<!DOCTYPE html>
<html>
<head>
	<title>Single upload : xw_upload</title>
</head>
<body>
	<?php if(isset($_GET['uploaded'])){ ?>
	<p>Your file has been uploaded : <a href="uploads/<?php echo $_GET['uploaded']; ?>"><?php echo $_GET['uploaded']; ?></a></p>
	<hr>
	<?php } ?>

	<form action="single_upload_process.php" method="POST" enctype="multipart/form-data">
		<p>
			<label>File input name</label>
			<input type="text" name="from" value="single_upload" disabled="disabled">			
		</p>

		<p>
			<label>Directory to upload</label>
			<input type="text" name="dir" value="uploads/" disabled="disabled">			
		</p>

		<p>
			<label>File name</label>
			<input type="text" name="file_name" placeholder="false">	
			<p>Default name : "up_" + unique id.</p>		
		</p>

		<p>
			<label>Max Size</label>
			<input type="number" name="max_size" placeholder="false"> mo
			<p>Default max size : 3Mo </p>		
		</p>

		<h2>Code</h2>
		<pre>
$upload = new xw_upload("single_upload", "uploads/");
$upload->single_upload(<span>false</span>, <span>false</span>);
		</pre>

		<p> <input type="file" name="single_upload" > </p>

		<p> <input type="submit" value="Upload file" name="submit"> </p>
	</form>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript">
		$('input[name="file_name"]').change(function(event) {
			if ($(this).val() == "") {
				$('pre').children('span:nth-child(1)').empty().append("false").css('color', '#000');
			} else {
				$('pre').children('span:nth-child(1)').empty().append($(this).val()).css('color', '#27ae60');
			}
		});

		$('input[name="max_size"]').change(function(event) {
			if ($(this).val() == "") {
				$('pre').children('span:nth-child(2)').empty().append("false").css('color', '#000');
			} else {
				$('pre').children('span:nth-child(2)').empty().append($(this).val()).css('color', '#27ae60');
			}
		});
	</script>
</body>
</html>
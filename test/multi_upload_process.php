<?php
ini_set('display_errors', '1');

require_once('../xw_upload.php');

$params = [
	"from" => "multi_upload",
	"dir" => "uploads/",
	"file_name" => false,
	"max_size" => false
];

if ( isset($_POST['file_name']) && $_POST['file_name'] != "" ){ $params["file_name"] = $_POST['file_name']; }
if ( isset($_POST['max_size']) && $_POST['max_size']   != "" ){ $params["max_size"] = $_POST['max_size']; }

$upload = new xw_upload($params['from'], $params['dir']);

try {
	$ups = $upload->multi_upload($params['file_name'], $params['max_size']);
	
	$params_str = "?uploaded=true&";
	foreach ($ups as $key => $value) {
		$params_str .= "file_" . $key . "=" . $value . "&";
	}
	header('Location: multi_upload.php' . $params_str );
} catch(Exception $e) {
	var_dump($e);
}
<!DOCTYPE html>
<html>
<head>
	<title>Multi upload : xw_upload</title>
</head>
<body>
	<?php if(isset($_GET['uploaded']) && $_GET['uploaded'] == true){ ?>
	<p>Your files has been uploaded : </p>
		<?php foreach($_GET as $key => $value){ ?>
			<?php if(strstr($key, "file_") != false) { ?>
				<p><a href="uploads/<?php echo $value; ?>"> <?php echo $value ?> </a></p>
			<?php } ?>
		<?php } ?>
	<hr>
	<?php } ?>

	<form action="multi_upload_process.php" method="POST" enctype="multipart/form-data">
		<p>
			<label>Files input name</label>
			<input type="text" name="from" value="multi_upload" disabled="disabled">			
		</p>

		<p>
			<label>Directory to upload</label>
			<input type="text" name="dir" value="uploads/" disabled="disabled">			
		</p>

		<p>
			<label>Files name</label>
			<input type="text" name="file_name" placeholder="false">		
			<p>Names will be : "<span id="file_name">up_</span>" + unique id.</p>		
		</p>

		<p>
			<label>Max Size</label>
			<input type="number" name="max_size" placeholder="false"> mo
			<p>Default max size : 3Mo </p>		
		</p>

		<h2>Code</h2>
		<pre>
$upload = new xw_upload("multi_upload", "uploads/");
$upload->multi_upload(<span>false</span>, <span>false</span>);
		</pre>

		<p> <input type="file" name="multi_upload[]" > </p>
		<p> <input type="file" name="multi_upload[]" > </p>

		<p> <input type="submit" value="Upload file" name="submit"> </p>
	</form>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript">
		$('input[name="file_name"]').change(function(event) {
			if ($(this).val() == "") {
				$('pre').children('span:nth-child(1)').empty().append("false").css('color', '#000');
				$('#file_name').empty().append("up_").css('color', '#000');
			} else {
				$('pre').children('span:nth-child(1)').empty().append($(this).val()).css('color', '#27ae60');
				$('#file_name').empty().append($(this).val()).css('color', '#000');
			}
		});

		$('input[name="max_size"]').change(function(event) {
			if ($(this).val() == "") {
				$('pre').children('span:nth-child(2)').empty().append("false").css('color', '#000');
			} else {
				$('pre').children('span:nth-child(2)').empty().append($(this).val()).css('color', '#27ae60');
			}
		});
	</script>
</body>
</html>
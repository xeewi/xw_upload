<?php
ini_set('display_errors', '1');

require_once('../xw_upload.php');

$params = [
	"from" => "single_upload",
	"dir" => "uploads/",
	"file_name" => false,
	"max_size" => false
];

if ( isset($_POST['file_name']) && $_POST['file_name'] != "" ){ $params["file_name"] = $_POST['file_name']; }
if ( isset($_POST['max_size']) && $_POST['max_size']   != "" ){ $params["max_size"] = $_POST['max_size']; }

$upload = new xw_upload($params['from'], $params['dir']);

try {
	$upload_file = $upload->single_upload($params['file_name'], $params['max_size']);
	header('Location: single_upload.php?uploaded=' . $upload_file );
} catch(Exception $e) {
	var_dump($e);
}